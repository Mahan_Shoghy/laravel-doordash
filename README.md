# Laravel Doordash API Package

> Laravel Doordash API Package simplifies integration with the Doordash API for food delivery services in Laravel applications.

For now, only the Drive (Delivery) functionality is released.

Feel free to add more features!

[Doordash API Documentation](https://developer.doordash.com/en-US/api/drive)

## Table of Contents
- [Installation](#installation)
- [Usage](#usage)
    - [Setup environment](#setup-environment)
    - [Delivery](#delivery)
    - [Webhook](#webhook)
- [Support](#support)
- [Donate](#donate)

## Installation

Via composer (Laravel 10+):

```shell
composer require mahan-shoghy/laravel-doordash
```

Optional: If you want to publish the config file, use the following command:

```shell
php artisan vendor:publish --provider="MahanShoghy\LaravelDoordash\DoordashServiceProvider" --tag="config"
```

## Usage

### Setup environment
Add and set these variables to your environment file:
```dotenv
DOORDASH_DEVELOPER_ID=
DOORDASH_KEY_ID=
DOORDASH_SIGNING_SECRET=
```
You can create credentials and get values from the Doordash Developer Portal

### Delivery

Use delivery facade:
```php
use MahanShoghy\LaravelDoordash\App\Drive\Facades\Delivery;
```

Create Quote -
[Documentation](https://developer.doordash.com/en-US/api/drive#tag/Delivery/operation/DeliveryQuote)
```php
Delivery::createQuote(array $data);
```

Accept Quote -
[Documentation](https://developer.doordash.com/en-US/api/drive#tag/Delivery/operation/DeliveryQuoteAccept)
```php
Delivery::acceptQuote(string $external_delivery_id, int $tip);
```

Create Delivery -
[Documentation](https://developer.doordash.com/en-US/api/drive#tag/Delivery/operation/CreateDelivery)
```php
Delivery::create(array $data);
```

Get Delivery -
[Documentation](https://developer.doordash.com/en-US/api/drive#tag/Delivery/operation/GetDelivery)
```php
Delivery::get(string $external_delivery_id);
```

Update Delivery -
[Documentation](https://developer.doordash.com/en-US/api/drive#tag/Delivery/operation/UpdateDelivery)
```php
Delivery::update(string $external_delivery_id, array $data);
```

Cancel Delivery -
[Documentation](https://developer.doordash.com/en-US/api/drive#tag/Delivery/operation/CancelDelivery)
```php
Delivery::cancel(string $external_delivery_id);
```

## Webhook

Webhooks allow your Laravel application to receive and handle Doordash events in real-time. Follow these steps to set up webhook handling in your application.

1. **Publish the Configuration File**: If you haven't already, publish the configuration file using the following command:
    ```shell
    php artisan vendor:publish --provider="MahanShoghy\LaravelDoordash\DoordashServiceProvider" --tag="config"
    ```
2. **Generate webhook authorization token**: Add the following variable to your environment file:

    ```dotenv
    DOORDASH_WEBHOOK_AUTH_TOKEN=
    ```

   Run the following command to generate token:

    ```shell
    php artisan doordash:generate-webhook-token
    ```

   The generated token will be set in your environment file. Use this token in the Authorization header when creating the webhook endpoint on Developer Portal.

3. **Define a Webhook Route**: In your Laravel routes file (like: web.php), add a route for handling webhooks. You can choose any URL you prefer. By default, the HTTP method is set to POST, but you can change it if needed:
    ```php
    Route::doordashWebhooks('your-webhook-url'); // Default method is POST
    // OR
    Route::doordashWebhooks('your-webhook-url', 'get'); // Change HTTP method to GET
    ```
4. **Configure Webhook Events**: Open the `doordash.php` configuration file (which you published in step 1) and specify the events you want to handle and the corresponding job classes. Here's an example:
    ```php
    'jobs' => [
        'DASHER_CONFIRMED' => DasherConfirmedJob::class,
    ]
    ```
   In the job class, you can access the payload data sent by Doordash to handle the event.
    ```php
    class DasherConfirmedJob implements ShouldQueue
    {
        public Payload $payload;
    
        public function __construct(Payload $payload)
        {
            $this->payload = $payload;
        }
    
        public function handle(): void
        {
            // Handle the webhook event using $this->payload
        }
    }
    ```
5. **Event Documentation**: You can find a list of available Doordash webhook events in the
   [Doordash documentation](https://developer.doordash.com/en-US/docs/drive/reference/webhooks#events). This resource will help you understand and configure the events you wish to handle.

6. **Use Laravel Queues**: For better performance and scalability, consider using Laravel queues to handle webhook jobs asynchronously. This ensures that your application remains responsive even during heavy webhook traffic.

Now, your Laravel application is ready to receive and process Doordash webhook events in real-time.

## Support

If you have any questions or need assistance, feel free to [contact us](mailto:mahan.shoghy7@gmail.com).
if there are specific code examples you'd like to include in the README to help users get started quickly, you can add those as well.

## Donate

If you find this package helpful and would like to support its development, you can make a donation in cryptocurrency. Your contributions will help maintain and improve this package.

You can send your contribution to the following address in your MetaMask wallet: <br>
Wallet Address:
```
0x4B3d57AC72B57a76b778Bac4e3b32d2d4d729955
```

Thank you for your support!

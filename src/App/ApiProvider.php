<?php

namespace MahanShoghy\LaravelDoordash\App;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;
use MahanShoghy\LaravelDoordash\JWT;

class ApiProvider
{
    protected string $jwt_token;

    protected const BASE_URL = 'https://openapi.doordash.com/';

    protected PendingRequest $http;

    public function __construct()
    {
        $this->jwt_token = JWT::getInstance()->getToken();

        $this->http = Http::acceptJson()->withToken($this->jwt_token);
    }
}

<?php

namespace MahanShoghy\LaravelDoordash\App\Drive;

use MahanShoghy\LaravelDoordash\App\ApiProvider;
use MahanShoghy\LaravelDoordash\App\Drive\Interfaces\DeliveryInterface;
use MahanShoghy\LaravelDoordash\App\Drive\Model\Delivery as DeliveryModel;
use MahanShoghy\LaravelDoordash\App\Drive\Parser\DeliveryParser;
use MahanShoghy\LaravelDoordash\DoordashException;

class Delivery extends ApiProvider implements DeliveryInterface
{
    public function createQuote(array $data): DeliveryModel
    {
        $response = $this->http->post(self::BASE_URL.'drive/v2/quotes', $data);

        if ($response->status() === 200){

            $delivery = $response->body();
            $delivery = json_decode($delivery, true);

            return (new DeliveryParser($delivery))->get();
        }

        throw new DoordashException($response);
    }

    public function acceptQuote(string $external_delivery_id, int $tip = 0): DeliveryModel
    {
        $response = $this->http->post(self::BASE_URL."drive/v2/quotes/$external_delivery_id/accept",
            compact('tip')
        );

        if ($response->status() === 200){

            $delivery = $response->body();
            $delivery = json_decode($delivery, true);

            return (new DeliveryParser($delivery))->get();
        }

        throw new DoordashException($response);
    }

    public function create(array $data): DeliveryModel
    {
        $response = $this->http->post(self::BASE_URL.'drive/v2/deliveries', $data);

        if ($response->status() === 200){

            $delivery = $response->body();
            $delivery = json_decode($delivery, true);

            return (new DeliveryParser($delivery))->get();
        }

        throw new DoordashException($response);
    }

    public function get(string $external_delivery_id): DeliveryModel
    {
        $response = $this->http->get(self::BASE_URL."drive/v2/deliveries/$external_delivery_id");

        if ($response->status() === 200){

            $delivery = $response->body();
            $delivery = json_decode($delivery, true);

            return (new DeliveryParser($delivery))->get();
        }

        throw new DoordashException($response);
    }

    public function update(string $external_delivery_id, array $data): DeliveryModel
    {
        $response = $this->http->patch(self::BASE_URL."drive/v2/deliveries/$external_delivery_id", $data);

        if ($response->status() === 200){

            $delivery = $response->body();
            $delivery = json_decode($delivery, true);

            return (new DeliveryParser($delivery))->get();
        }

        throw new DoordashException($response);
    }

    public function cancel(string $external_delivery_id): DeliveryModel
    {
        $response = $this->http->put(self::BASE_URL."drive/v2/deliveries/$external_delivery_id/cancel");

        if ($response->status() === 200){

            $delivery = $response->body();
            $delivery = json_decode($delivery, true);

            return (new DeliveryParser($delivery))->get();
        }

        throw new DoordashException($response);
    }
}

<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Enums;

use MahanShoghy\PhpEnumHelper\EnumHelper;

enum ActionIfUndeliverableEnum: string
{
    use EnumHelper;

    case RETURN_TO_PICKUP = 'return_to_pickup';
    case DISPOSE = 'dispose';
}

<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Enums;

use MahanShoghy\PhpEnumHelper\EnumHelper;

enum CancellationReasonEnum: string
{
    use EnumHelper;

    case CANCEL_BY_DISPATCH = 'cancel_by_dispatch';
    case CANCEL_BY_MERCHANT = 'cancel_by_merchant';
    case CANCEL_BY_ORDER_PLACER = 'cancel_by_order_placer';
    case CUSTOMER_REQUESTED_OTHER = 'customer_requested_other';
    case DASHER_CANNOT_FULFILL_OTHER = 'dasher_cannot_fulfill_other';
    case DASHER_NOT_RESPONDING = 'dasher_not_responding';
    case DRIVE_ORDER_PICKED_UP_BY_CUSTOMER = 'drive_order_picked_up_by_customer';
    case DUPLICATE_ORDER = 'duplicate_order';
    case FRAUDULENT_ORDER = 'fraudulent_order';
    case ITEMS_TEMP_UNAVAILABLE = 'items_temp_unavailable';
    case NO_AVAILABLE_DASHERS = 'no_available_dashers';
    case NONTABLET_PROTOCOL_ISSUE = 'nontablet_protocol_issue';
    case OTHER = 'other';
    case PICKED_UP_BY_OTHER_DASHER = 'picked_up_by_other_dasher';
    case STORE_CANNOT_FULFILL_OTHER = 'store_cannot_fulfill_other';
    case STORE_CLOSED = 'store_closed';
    case TEST_ORDER = 'test_order';
    case TOO_BUSY = 'too_busy';
    case TOO_LATE = 'too_late';
    case WRONG_DELIVERY_ADDRESS = 'wrong_delivery_address';
    case PACKAGE_NEEDS_REDELIVERY = 'package_needs_redelivery';
    case PACKAGE_NEVER_RECEIVED = 'package_never_received';
    case PACKAGE_LOST_AT_FACILITY = 'package_lost_at_facility';


    public function comment(): string
    {
        return match($this) {
            self::CANCEL_BY_DISPATCH => "Order was cancelled by DoorDash support",
            self::CANCEL_BY_MERCHANT => "The order was cancelled by the merchant",
            self::CANCEL_BY_ORDER_PLACER => "The order was cancelled by the person that created it",
            self::CUSTOMER_REQUESTED_OTHER => "The customer cancelled the order",
            self::DASHER_CANNOT_FULFILL_OTHER => "The Dasher couldn't fulfill the order",
            self::DASHER_NOT_RESPONDING => "The Dasher was not responding",
            self::DRIVE_ORDER_PICKED_UP_BY_CUSTOMER => "The order was picked up by the customer",
            self::DUPLICATE_ORDER => "The order is a duplicate of another order",
            self::FRAUDULENT_ORDER => "DoorDash suspects this order is fraudulent",
            self::ITEMS_TEMP_UNAVAILABLE => "Items were temporarily unavailable",
            self::NO_AVAILABLE_DASHERS => "No Dashers are available for this order",
            self::NONTABLET_PROTOCOL_ISSUE => "DoorDash didn't receive the full order",
            self::OTHER => "DoorDash encountered an unknown error",
            self::PICKED_UP_BY_OTHER_DASHER => "Order was picked up by another dasher",
            self::STORE_CANNOT_FULFILL_OTHER => "The store couldn't fulfill the order",
            self::STORE_CLOSED => "The store is not available at the time the order is requested or was closed when the Dasher arrived",
            self::TEST_ORDER => "The order was a test order and was cancelled by a clean-up job",
            self::TOO_BUSY => "The restaurant is too busy",
            self::TOO_LATE => "The order was taking too long",
            self::WRONG_DELIVERY_ADDRESS => "The delivery address was incorrect",
            self::PACKAGE_NEEDS_REDELIVERY => "The package delivery attempt failed and will be redelivered",
            self::PACKAGE_NEVER_RECEIVED => "The package was never received at dashmart",
            self::PACKAGE_LOST_AT_FACILITY => "The package was lost after receiving at dashmart",

        };
    }
}

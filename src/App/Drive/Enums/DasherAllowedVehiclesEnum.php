<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Enums;

use MahanShoghy\PhpEnumHelper\EnumHelper;

enum DasherAllowedVehiclesEnum: string
{
    use EnumHelper;

    case CAR = 'car';
    case BICYCLE = 'bicycle';
    case WALKING = 'walking';
}

<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Enums;

use MahanShoghy\PhpEnumHelper\EnumHelper;

enum DeliveryStatusEnum: string
{
    use EnumHelper;

    case CREATED = 'created'; // The delivery has been created and is waiting assignment to a Dasher.
    case CONFIRMED = 'confirmed'; // The delivery has been assigned to a Dasher and the Dasher has confirmed the delivery.
    case ENROUTE_TO_PICKUP = 'enroute_to_pickup'; // The Dasher is en route to the pick-up location.
    case ARRIVED_AT_PICKUP = 'arrived_at_pickup'; // The Dasher has arrived at the pick-up location.
    case PICKED_UP = 'picked_up'; // The Dasher has picked up the items and is heading to the drop off.
    case ENROUTE_TO_DROPOFF = 'enroute_to_dropoff'; // The Dasher is en route to the drop-off location.
    case ARRIVED_AT_DROPOFF = 'arrived_at_dropoff'; // The Dasher has arrived at the drop-off location.
    case DELIVERED = 'delivered'; // The delivery has been completed.
    case CANCELLED = 'cancelled'; // The delivery has been cancelled.
}

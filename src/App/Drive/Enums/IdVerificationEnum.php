<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Enums;

use MahanShoghy\PhpEnumHelper\EnumHelper;

enum IdVerificationEnum: string
{
    use EnumHelper;

    case REQUIRED = 'required';
    case PREFERRED = 'preferred';
    case NONE = 'none';
}

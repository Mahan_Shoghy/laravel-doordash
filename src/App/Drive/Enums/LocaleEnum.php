<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Enums;

use MahanShoghy\PhpEnumHelper\EnumHelper;

enum LocaleEnum: string
{
    use EnumHelper;

    case EN = 'en-US';
    case FR = 'fr-CA';
    case ES = 'es-US';
}

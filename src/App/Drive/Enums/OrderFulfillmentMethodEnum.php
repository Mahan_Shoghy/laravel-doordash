<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Enums;

use MahanShoghy\PhpEnumHelper\EnumHelper;

enum OrderFulfillmentMethodEnum: string
{
    use EnumHelper;

    case STANDARD = "standard";
    case CATERING = "catering";
    case SHOP_STAGE = "shop_stage";
    case SHOP_DELIVER = "shop_deliver";
    case PARCEL = "parcel";
}

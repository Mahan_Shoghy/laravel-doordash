<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Enums;

use MahanShoghy\PhpEnumHelper\EnumHelper;

enum PaymentMethodEnum: string
{
    use EnumHelper;

    case RED_CARD = "red_card";
    case BARCODE = "barcode";
    case SHOP_CARD = "shop_card";
    case GIFT_CARDS = "gift_cards";
}

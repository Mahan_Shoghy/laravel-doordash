<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Enums;

use MahanShoghy\PhpEnumHelper\EnumHelper;

enum ProofOfDeliveryEnum: string
{
    use EnumHelper;

    case PHOTO_REQUIRED = "photo_required";
    case PHOTO_PREFERRED = "photo_preferred";
    case PIN_CODE = "pin_code";
    case NONE = "none";
}

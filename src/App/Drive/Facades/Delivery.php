<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Facades;

use Illuminate\Support\Facades\Facade;
use MahanShoghy\LaravelDoordash\App\Drive\Model\Delivery as DeliveryModel;


/**
 * @method static DeliveryModel createQuote(array $data)
 * @var array $data
 * Array of parameters.
 * Reference: https://developer.doordash.com/en-US/api/drive/#tag/Delivery/operation/DeliveryQuote
 *
 * @method static DeliveryModel acceptQuote(string $external_delivery_id, int $tip = 0)
 * @var string $external_delivery_id
 * string/[a-zA-Z0-9-._~]+/
 * Example: D-1763
 * Unique (per developer) ID of the delivery.
 * Reference: https://developer.doordash.com/en-US/api/drive/#tag/Delivery/operation/DeliveryQuoteAccept
 * @var int $tip
 * integer <int32> (Tip) >= 0
 * The tip amount. Use cents or the equivalent lowest currency denomination (e.g. $5.99 = 599).
 *
 * @method static DeliveryModel create(array $data)
 * @var array $data
 * Array of parameters.
 * Reference: https://developer.doordash.com/en-US/api/drive/#tag/Delivery/operation/CreateDelivery
 *
 * @method static DeliveryModel get(string $external_delivery_id)
 * @param string $external_delivery_id
 * string/[a-zA-Z0-9-._~]+/
 * Example: D-1763
 * Unique (per developer) ID of the delivery.
 * Reference: https://developer.doordash.com/en-US/api/drive/#tag/Delivery/operation/GetDelivery
 *
 * @method static DeliveryModel update(string $external_delivery_id, array $data)
 * @var array $data
 * Array of parameters.
 * Reference: https://developer.doordash.com/en-US/api/drive/#tag/Delivery/operation/UpdateDelivery
 * @param string $external_delivery_id
 * string/[a-zA-Z0-9-._~]+/
 * Example: D-1763
 * Unique (per developer) ID of the delivery.
 *
 * @method static DeliveryModel cancel(string $external_delivery_id)
 * @param string $external_delivery_id
 * string/[a-zA-Z0-9-._~]+/
 * Example: D-1763
 * Unique (per developer) ID of the delivery.
 * Reference: https://developer.doordash.com/en-US/api/drive/#tag/Delivery/operation/CancelDelivery
 */
class Delivery extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return \MahanShoghy\LaravelDoordash\App\Drive\Delivery::class;
    }
}

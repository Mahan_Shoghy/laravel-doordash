<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Interfaces;

use MahanShoghy\LaravelDoordash\App\Drive\Model\Delivery;
use MahanShoghy\LaravelDoordash\DoordashException;

interface DeliveryInterface
{
    /**
     * @reference https://developer.doordash.com/en-US/api/drive/#tag/Delivery/operation/DeliveryQuote
     * @param array $data
     * @return Delivery
     * @throws DoordashException
     */
    public function createQuote(array $data): Delivery;

    /**
     * @reference https://developer.doordash.com/en-US/api/drive/#tag/Delivery/operation/DeliveryQuoteAccept
     * @param string $external_delivery_id
        string/[a-zA-Z0-9-._~]+/
        Example: D-1763
        Unique (per developer) ID of the delivery.

     * @param int $tip
        integer <int32> (Tip) >= 0
        The tip amount. Use cents or the equivalent lowest currency denomination (e.g. $5.99 = 599).

     * @return Delivery
     * @throws DoordashException
     */
    public function acceptQuote(string $external_delivery_id, int $tip = 0): Delivery;

    /**
     * @reference https://developer.doordash.com/en-US/api/drive/#tag/Delivery/operation/CreateDelivery
     * @param array $data
     * @return Delivery
     * @throws DoordashException
     */
    public function create(array $data): Delivery;

    /**
     * @reference https://developer.doordash.com/en-US/api/drive/#tag/Delivery/operation/GetDelivery
     * @param string $external_delivery_id
        string/[a-zA-Z0-9-._~]+/
        Example: D-1763
        Unique (per developer) ID of the delivery.

     * @return Delivery
     * @throws DoordashException
     */
    public function get(string $external_delivery_id): Delivery;

    /**
     * @reference https://developer.doordash.com/en-US/api/drive/#tag/Delivery/operation/UpdateDelivery
     * @param string $external_delivery_id
        string/[a-zA-Z0-9-._~]+/
        Example: D-1763
        Unique (per developer) ID of the delivery.
     * @param array $data
     * @return Delivery
     * @throws DoordashException
     */
    public function update(string $external_delivery_id, array $data): Delivery;

    /**
     * @reference https://developer.doordash.com/en-US/api/drive/#tag/Delivery/operation/CancelDelivery
     * @param string $external_delivery_id
        string/[a-zA-Z0-9-._~]+/
        Example: D-1763
        Unique (per developer) ID of the delivery.

     * @return Delivery
     * @throws DoordashException
     */
    public function cancel(string $external_delivery_id): Delivery;
}

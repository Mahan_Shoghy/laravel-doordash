<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Interfaces;

interface ParserInterface
{
    public function __construct(array $data);

    public function get();
}

<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Model;

class DasherLocation
{
    /**
     * @param float $lat
     * @param float $lng
     */
    public function __construct(
        public readonly float $lat,
        public readonly float $lng
    ){}
}

<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Model;

use Carbon\Carbon;
use MahanShoghy\LaravelDoordash\App\Drive\Enums\ActionIfUndeliverableEnum;
use MahanShoghy\LaravelDoordash\App\Drive\Enums\CancellationReasonEnum;
use MahanShoghy\LaravelDoordash\App\Drive\Enums\DeliveryStatusEnum;
use MahanShoghy\LaravelDoordash\App\Drive\Enums\LocaleEnum;
use MahanShoghy\LaravelDoordash\App\Drive\Enums\OrderFulfillmentMethodEnum;

class Delivery
{
    /**
     * @param string $external_delivery_id example: "D-1763"
     * @param LocaleEnum|null $locale example: "en-US, fr-CA, es-US"
     * @param OrderFulfillmentMethodEnum|null $order_fulfillment_method
     * @param string|null $origin_facility_id example: "MERCHANTA-CA-1"
     * @param string|null $pickup_address example: "901 Market Street 6th Floor San Francisco, CA 94103"
     * @param string|null $pickup_business_name example: "Wells Fargo SF Downtown"
     * @param string|null $pickup_phone_number example: "+16505555555"
     * @param string|null $pickup_instructions example: "Go to the bar for pick up."
     * @param string|null $pickup_reference_tag example: "Order number 61"
     * @param string|null $pickup_external_business_id example: "ase-243-dzs"
     * @param string|null $pickup_external_store_id example: "ase-243-dzs"
     * @param string $dropoff_address example: "901 Market Street 6th Floor San Francisco, CA 94103"
     * @param string|null $dropoff_business_name example: "The Avery Condominium"
     * @param array|null $dropoff_location example: ["lat" => 123.1312343, "lng" => -37.2144343]
     * @param string $dropoff_phone_number example: "+16505555555"
     * @param string|null $dropoff_instructions example: "Enter gate code 1234 on the callbox."
     * @param string|null $dropoff_contact_given_name example: "John"
     * @param string|null $dropoff_contact_family_name example: "Doe"
     * @param true|null $dropoff_contact_send_notifications example:
     * @param array|null $dropoff_options example: ["signature" => "required", "id_verification" => "required", "proof_of_delivery" => "photo_required"]
     * @param int|null $order_value example: 1999
     * @param string|null $currency example: "USD"
     * @param array|null $items example:
     * @param ShoppingOptions|null $shopping_options example: {"payment_method": "red_card","payment_barcode": "12345","payment_gift_cards": ["123443434","123443435"],"ready_for_pickup_by": "2018-08-22T17:20:28Z","dropoff_contact_loyalty_number": "1234-5678-9876-5432-1"}
     * @param DeliveryStatusEnum|null $delivery_status example: "quote"
     * @param CancellationReasonEnum|null $cancellation_reason
     * @param Carbon|null $updated_at example: "2018-08-22T17:20:28Z"
     * @param Carbon|null $pickup_time_estimated example: "2018-08-22T17:20:28Z"
     * @param Carbon|null $pickup_time_actual example: "2018-08-22T17:20:28Z"
     * @param Carbon|null $dropoff_time_estimated example: "2018-08-22T17:20:28Z"
     * @param Carbon|null $dropoff_time_actual example: "2018-08-22T17:20:28Z"
     * @param Carbon|null $return_time_estimated example: "2018-08-22T17:20:28Z"
     * @param Carbon|null $return_time_actual example: "2018-08-22T17:20:28Z"
     * @param string|null $return_address example: "901 Market Street 6th Floor San Francisco, CA 94103"
     * @param int|null $fee example: 1900
     * @param array|null $fee_components example: [["type": "distance_based_fee","amount": 1900]]
     * @param int|null $tax example: 520
     * @param array|null $tax_components example: [["type": "gst_hst","amount": 520]]
     * @param string|null $support_reference example: "86313"
     * @param string|null $tracking_url example: "https://doordash.com/tracking?id="
     * @param string|null $dropoff_verification_image_url example: "https://doordash-static.s3..."
     * @param string|null $pickup_verification_image_url example: "https://doordash-static.s3..."
     * @param string|null $dropoff_signature_image_url example: "https://doordash-static.s3..."
     * @param ShippingLabel|null $shipping_label
     * @param array|null $dropped_items example: [["external_id": "1011902870","type": "main_item","reason": "item_not_found_in_catalog"]]
     * @param bool $contactless_dropoff
     * @param ActionIfUndeliverableEnum|null $action_if_undeliverable example:
     * @param int|null $tip example: 599
     * @param OrderContains|null $order_contains example: ["alcohol": false]
     * @param array|null $dasher_allowed_vehicles
     * @param false|null $dropoff_requires_signature
     * @param string|null $promotion_id example: "ee680b87-0016-496e-ac3c-d3f33ab54c1c"
     * @param int|null $dropoff_cash_on_delivery example: 1999
     * @param int|null $dasher_id example: 1232142
     * @param string|null $dasher_name example: "John D."
     * @param string|null $dasher_dropoff_phone_number example: "+15555555555"
     * @param string|null $dasher_pickup_phone_number example: "+14444444444"
     * @param DasherLocation|null $dasher_location
     * @param string|null $dasher_vehicle_make example: "Toyota"
     * @param string|null $dasher_vehicle_model example: "Corolla"
     * @param string|null $dasher_vehicle_year example: "2006"
     */
    public function __construct(
        public readonly string $external_delivery_id,
        public readonly ?LocaleEnum $locale,
        public readonly ?OrderFulfillmentMethodEnum $order_fulfillment_method,
        public readonly ?string $origin_facility_id,
        public readonly ?string $pickup_address,
        public readonly ?string $pickup_business_name,
        public readonly ?string $pickup_phone_number,
        public readonly ?string $pickup_instructions,
        public readonly ?string $pickup_reference_tag,
        public readonly ?string $pickup_external_business_id,
        public readonly ?string $pickup_external_store_id,
        public readonly string $dropoff_address,
        public readonly ?string $dropoff_business_name,
        public readonly ?array $dropoff_location,
        public readonly string $dropoff_phone_number,
        public readonly ?string                 $dropoff_instructions,
        public readonly ?string                 $dropoff_contact_given_name,
        public readonly ?string                 $dropoff_contact_family_name,
        public readonly ?bool                   $dropoff_contact_send_notifications,
        public readonly ?array                  $dropoff_options,
        public readonly ?int                    $order_value,
        public readonly ?string                 $currency,
        public readonly ?array                  $items,
        public readonly ?ShoppingOptions        $shopping_options,
        public readonly ?DeliveryStatusEnum     $delivery_status,
        public readonly ?CancellationReasonEnum $cancellation_reason,
        public readonly ?Carbon                 $updated_at,
        public readonly ?Carbon                 $pickup_time_estimated,
        public readonly ?Carbon                 $pickup_time_actual,
        public readonly ?Carbon                 $dropoff_time_estimated,
        public readonly ?Carbon                 $dropoff_time_actual,
        public readonly ?Carbon                 $return_time_estimated,
        public readonly ?Carbon                 $return_time_actual,
        public readonly ?string                 $return_address,
        public readonly ?int                    $fee,
        public readonly ?array                  $fee_components,
        public readonly ?int $tax,
        public readonly ?array $tax_components,
        public readonly ?string $support_reference,
        public readonly ?string $tracking_url,
        public readonly ?string $dropoff_verification_image_url,
        public readonly ?string $pickup_verification_image_url,
        public readonly ?string $dropoff_signature_image_url,
        public readonly ?ShippingLabel $shipping_label,
        public readonly ?array $dropped_items,
        public readonly ?bool $contactless_dropoff,
        public readonly ?ActionIfUndeliverableEnum $action_if_undeliverable,
        public readonly ?int $tip,
        public readonly ?OrderContains $order_contains,
        public readonly ?array $dasher_allowed_vehicles,
        public readonly ?bool $dropoff_requires_signature,
        public readonly ?string $promotion_id,
        public readonly ?int $dropoff_cash_on_delivery,
        public readonly ?int $dasher_id,
        public readonly ?string $dasher_name,
        public readonly ?string $dasher_dropoff_phone_number,
        public readonly ?string $dasher_pickup_phone_number,
        public readonly ?DasherLocation $dasher_location,
        public readonly ?string $dasher_vehicle_make,
        public readonly ?string $dasher_vehicle_model,
        public readonly ?string $dasher_vehicle_year
    ){}
}

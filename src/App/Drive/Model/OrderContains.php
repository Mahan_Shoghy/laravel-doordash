<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Model;

class OrderContains
{
    /**
     * @param bool $alcohol
     */
    public function __construct(
        public readonly bool $alcohol = false
    ){}
}

<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Model;

class ShippingLabel
{
    /**
     * @param string|null $label_format example: "zpl"
     * @param string|null $label_size example: "4x6"
     * @param string|null $print_density example: "203dpi"
     * @param string|null $label_string example: "XlhBCl5DRjAsNjAKXkZPNTAsNTBeRkRTdG9yZU5hbWVeRlMKX"
     */
    public function __construct(
        public readonly ?string $label_format,
        public readonly ?string $label_size,
        public readonly ?string $print_density,
        public readonly ?string $label_string
    ){}
}

<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Model;

use Carbon\Carbon;
use MahanShoghy\LaravelDoordash\App\Drive\Enums\PaymentMethodEnum;

class ShoppingOptions
{
    /**
     * @param PaymentMethodEnum $payment_method example: "red_card"
     * @param string|null $payment_barcode example: "12345"
     * @param array|null $payment_gift_cards example: ["123443434", "123443435"]
     * @param Carbon|null $ready_for_pickup_by example: "2018-08-22T17:20:28Z"
     * @param string $dropoff_contact_loyalty_number example: "1234-5678-9876-5432-1"
     */
    public function __construct(
        public readonly PaymentMethodEnum $payment_method,
        public readonly ?string $payment_barcode,
        public readonly ?array $payment_gift_cards,
        public readonly ?Carbon $ready_for_pickup_by,
        public readonly string $dropoff_contact_loyalty_number
    ){}
}

<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Parser;

use MahanShoghy\LaravelDoordash\App\Drive\Interfaces\ParserInterface;
use MahanShoghy\LaravelDoordash\App\Drive\Model\DasherLocation;

class DasherLocationParser implements ParserInterface
{
    private DasherLocation $dasher_location;

    public function __construct(array $data)
    {
        $this->dasher_location = new DasherLocation($data['lat'], $data['lng']);
    }

    public function get(): DasherLocation
    {
        return $this->dasher_location;
    }
}

<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Parser;

use Carbon\Carbon;
use MahanShoghy\LaravelDoordash\App\Drive\Enums\ActionIfUndeliverableEnum;
use MahanShoghy\LaravelDoordash\App\Drive\Enums\CancellationReasonEnum;
use MahanShoghy\LaravelDoordash\App\Drive\Enums\DeliveryStatusEnum;
use MahanShoghy\LaravelDoordash\App\Drive\Enums\LocaleEnum;
use MahanShoghy\LaravelDoordash\App\Drive\Enums\OrderFulfillmentMethodEnum;
use MahanShoghy\LaravelDoordash\App\Drive\Interfaces\ParserInterface;
use MahanShoghy\LaravelDoordash\App\Drive\Model\Delivery;

class DeliveryParser implements ParserInterface
{
    private Delivery $delivery;

    public function __construct(array $data)
    {
        $locale = (empty($data['locale']))
            ? null
            : LocaleEnum::getFrom($data['locale']);

        $order_fulfillment_method = (empty($data['order_fulfillment_method']))
            ? null
            : OrderFulfillmentMethodEnum::getFrom($data['order_fulfillment_method']);

        $action_if_undeliverable = (empty($data['action_if_undeliverable']))
            ? null
            : ActionIfUndeliverableEnum::getFrom($data['action_if_undeliverable']);

        $shopping_options = (empty($data['shopping_options']))
            ? null
            : (new ShoppingOptionParser($data['shopping_options']))->get();

        $delivery_status = (empty($data['delivery_status']))
            ? null
            : DeliveryStatusEnum::getFrom($data['delivery_status']);

        $cancellation_reason = (empty($data['cancellation_reason']))
            ? null
            : CancellationReasonEnum::getFrom($data['cancellation_reason']);

        $updated_at = (empty($data['updated_at']))
            ? null
            : Carbon::parse($data['updated_at']);

        $pickup_time_estimated = (empty($data['pickup_time_estimated']))
            ? null
            : Carbon::parse($data['pickup_time_estimated']);

        $pickup_time_actual = (empty($data['pickup_time_actual']))
            ? null
            : Carbon::parse($data['pickup_time_actual']);

        $dropoff_time_estimated = (empty($data['dropoff_time_estimated']))
            ? null
            : Carbon::parse($data['dropoff_time_estimated']);

        $dropoff_time_actual = (empty($data['dropoff_time_actual']))
            ? null
            : Carbon::parse($data['dropoff_time_actual']);

        $return_time_estimated = (empty($data['return_time_estimated']))
            ? null
            : Carbon::parse($data['return_time_estimated']);

        $return_time_actual = (empty($data['return_time_actual']))
            ? null
            : Carbon::parse($data['return_time_actual']);

        $return_address = (empty($data['return_address']))
            ? null
            : Carbon::parse($data['return_address']);

        $shipping_label = (empty($data['shipping_label']))
            ? null
            : (new ShippingLabelParser($data['shipping_label']))->get();

        $order_contains = (empty($data['order_contains']))
            ? null
            : (new OrderContainsParser($data['order_contains']))->get();

        $dasher_location = (empty($data['dasher_location']))
            ? null
            : (new DasherLocationParser($data['dasher_location']))->get();


        $this->delivery = new Delivery(
            $data['external_delivery_id'] ?? null,
            $locale,
            $order_fulfillment_method,
            $data['origin_facility_id'] ?? null,
            $data['pickup_address'] ?? null,
            $data['pickup_business_name'] ?? null,
            $data['pickup_phone_number'] ?? null,
            $data['pickup_instructions'] ?? null,
            $data['pickup_reference_tag'] ?? null,
            $data['pickup_external_business_id'] ?? null,
            $data['pickup_external_store_id'] ?? null,
            $data['dropoff_address'] ?? null,
            $data['dropoff_business_name'] ?? null,
            $data['dropoff_location'] ?? null,
            $data['dropoff_phone_number'] ?? null,
            $data['dropoff_instructions'] ?? null,
            $data['dropoff_contact_given_name'] ?? null,
            $data['dropoff_contact_family_name'] ?? null,
            $data['dropoff_contact_send_notifications'] ?? null,
            $data['dropoff_options'] ?? null,
            $data['order_value'] ?? null,
            $data['currency'] ?? null,
            $data['items'] ?? null,
            $shopping_options,
            $delivery_status,
            $cancellation_reason,
            $updated_at,
            $pickup_time_estimated,
            $pickup_time_actual,
            $dropoff_time_estimated,
            $dropoff_time_actual,
            $return_time_estimated,
            $return_time_actual,
            $return_address,
            $data['fee'] ?? null,
            $data['fee_components'] ?? null,
            $data['tax'] ?? null,
            $data['tax_components'] ?? null,
            $data['support_reference'] ?? null,
            $data['tracking_url'] ?? null,
            $data['dropoff_verification_image_url'] ?? null,
            $data['pickup_verification_image_url'] ?? null,
            $data['dropoff_signature_image_url'] ?? null,
            $shipping_label,
            $data['dropped_items'] ?? null,
            $data['contactless_dropoff'] ?? null,
            $action_if_undeliverable,
            $data['tip'] ?? null,
            $order_contains,
            $data['dasher_allowed_vehicles'],
            $data['dropoff_requires_signature'] ?? null,
            $data['promotion_id'] ?? null,
            $data['dropoff_cash_on_delivery'] ?? null,
            $data['dasher_id'] ?? null,
            $data['dasher_name'] ?? null,
            $data['dasher_dropoff_phone_number'] ?? null,
            $data['dasher_pickup_phone_number'] ?? null,
            $dasher_location,
            $data['dasher_vehicle_make'] ?? null,
            $data['dasher_vehicle_model'] ?? null,
            $data['dasher_vehicle_year'] ?? null
        );
    }

    public function get(): Delivery
    {
        return $this->delivery;
    }
}

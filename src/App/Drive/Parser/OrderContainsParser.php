<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Parser;

use MahanShoghy\LaravelDoordash\App\Drive\Interfaces\ParserInterface;
use MahanShoghy\LaravelDoordash\App\Drive\Model\OrderContains;

class OrderContainsParser implements ParserInterface
{
    private OrderContains $order_contains;

    public function __construct(array $data)
    {
        $this->order_contains = new OrderContains($data['alcohol']);
    }

    public function get(): OrderContains
    {
        return $this->order_contains;
    }
}

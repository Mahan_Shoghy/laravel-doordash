<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Parser;

use MahanShoghy\LaravelDoordash\App\Drive\Interfaces\ParserInterface;
use MahanShoghy\LaravelDoordash\App\Drive\Model\ShippingLabel;

class ShippingLabelParser implements ParserInterface
{
    private ShippingLabel $shipping_label;

    public function __construct(array $data)
    {
        $this->shipping_label = new ShippingLabel(
            $data['label_format'],
            $data['label_size'],
            $data['print_density'],
            $data['label_string']
        );
    }

    public function get(): ShippingLabel
    {
        return $this->shipping_label;
    }
}

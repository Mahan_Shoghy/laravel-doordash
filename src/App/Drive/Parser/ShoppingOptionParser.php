<?php

namespace MahanShoghy\LaravelDoordash\App\Drive\Parser;

use Carbon\Carbon;
use MahanShoghy\LaravelDoordash\App\Drive\Enums\PaymentMethodEnum;
use MahanShoghy\LaravelDoordash\App\Drive\Interfaces\ParserInterface;
use MahanShoghy\LaravelDoordash\App\Drive\Model\ShoppingOptions;

class ShoppingOptionParser implements ParserInterface
{
    private ShoppingOptions $shopping_options;

    public function __construct(array $data)
    {
        $this->shopping_options = new ShoppingOptions(
            PaymentMethodEnum::getFrom($data['payment_method']),
            $data['payment_barcode'],
            $data['payment_gift_cards'],
            Carbon::parse($data['ready_for_pickup_by']),
            $data['dropoff_contact_loyalty_number']
        );
    }

    public function get(): ShoppingOptions
    {
        return $this->shopping_options;
    }
}

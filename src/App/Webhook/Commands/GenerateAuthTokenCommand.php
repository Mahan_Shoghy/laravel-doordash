<?php

namespace MahanShoghy\LaravelDoordash\App\Webhook\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Support\Str;

class GenerateAuthTokenCommand extends Command
{
    use ConfirmableTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doordash:generate-webhook-token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create and set doordash webhook token';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $token = Str::random(32);
        $token = bcrypt($token);

        if (! $this->setKeyInEnvironmentFile($token)) {
            return;
        }

        $this->laravel['config']['doordash.webhook_auth_token'] = $token;

        $this->line('<comment>'.$token.'</comment>');
        $this->info('Token successfully set in environment!');
    }

    protected function setKeyInEnvironmentFile($token): bool
    {
        $currentKey = $this->laravel['config']['doordash.webhook_auth_token'];

        if (strlen($currentKey) !== 0 && (! $this->confirmToProceed())) {
            return false;
        }

        if (! $this->writeNewEnvironmentFileWith($token)) {
            return false;
        }

        return true;
    }

    /**
     * Write a new environment file with the given key.
     *
     * @param string $token
     * @return bool
     */
    protected function writeNewEnvironmentFileWith(string $token): bool
    {
        $replaced = preg_replace(
            $this->keyReplacementPattern(),
            'DOORDASH_WEBHOOK_AUTH_TOKEN='.$token,
            $input = file_get_contents($this->laravel->environmentFilePath())
        );

        if ($replaced === $input || $replaced === null) {
            $this->error('Unable to set token. No DOORDASH_WEBHOOK_AUTH_TOKEN variable was found in the .env file.');

            return false;
        }

        file_put_contents($this->laravel->environmentFilePath(), $replaced);

        return true;
    }

    /**
     * Get a regex pattern that will match env APP_KEY with any random key.
     *
     * @return string
     */
    protected function keyReplacementPattern(): string
    {
        $escaped = preg_quote('='.$this->laravel['config']['doordash.webhook_auth_token'], '/');

        return "/^DOORDASH_WEBHOOK_AUTH_TOKEN{$escaped}/m";
    }
}

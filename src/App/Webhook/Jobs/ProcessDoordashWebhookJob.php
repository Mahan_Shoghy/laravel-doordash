<?php

namespace MahanShoghy\LaravelDoordash\App\Webhook\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use MahanShoghy\LaravelDoordash\App\Webhook\Model\Payload;

class ProcessDoordashWebhookJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(public Payload $payload){}

    public function handle(): void
    {
        $job_class = $this->determineJobClass($this->payload->event_name);

        if ($job_class){
            dispatch(new $job_class($this->payload));
        }
    }

    private function determineJobClass(string $event_name): ?string
    {
        return config("doordash.jobs.{$event_name}", null);
    }
}

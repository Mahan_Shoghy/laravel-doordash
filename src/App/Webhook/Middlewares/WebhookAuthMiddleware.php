<?php

namespace MahanShoghy\LaravelDoordash\App\Webhook\Middlewares;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class WebhookAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return Response
     */
    public function handle(Request $request, Closure $next): Response
    {
        $token = config('doordash.webhook_auth_token');

        if (! $token || $request->header('Authorization') === $token) {
            return $next($request);
        }

        abort(Response::HTTP_UNAUTHORIZED);
    }
}

<?php

namespace MahanShoghy\LaravelDoordash\App\Webhook\Model;

use Carbon\Carbon;
use MahanShoghy\LaravelDoordash\App\Drive\Enums\CancellationReasonEnum;
use MahanShoghy\LaravelDoordash\App\Drive\Model\DasherLocation;

class Payload
{
    /**
     * @param Carbon $created_at example: "2022-02-01T23:18:22.791883Z"
     * @param string $event_name example: "DASHER_DROPPED_OFF"
     * @param string $external_delivery_id example: "c19a5d37-e457-4247-9a67-921ec0134125"
     * @param int|null $dasher_id example: 123212
     * @param string|null $dasher_name example: "John D."
     * @param string|null $dasher_dropoff_phone_number example: "+16504379788"
     * @param string|null $dasher_pickup_phone_number example: "+16504379799"
     * @param DasherLocation|null $dasher_location
     * @param string|null $dasher_vehicle_make example: "Honda"
     * @param string|null $dasher_vehicle_model example: "Civic"
     * @param string|null $dasher_vehicle_year example: "2003"
     * @param string|null $pickup_address example: "1000 4th Avenue, Seattle, WA 98104"
     * @param string|null $pickup_phone_number example: "+1(855)9731040"
     * @param string|null $pickup_instructions example: "please take it to floor 21"
     * @param string|null $pickupReferenceTag example: "reftag"
     * @param string|null $pickup_external_business_id example: "ase-243-dzs"
     * @param string|null $pickup_external_store_id example: "ase-243-dzs"
     * @param string|null $dropoff_address example: "1201 3rd Avenue, Seattle, WA 98101"
     * @param string|null $dropoff_phone_number example: "+1(855)9731040"
     * @param string|null $dropoff_instructions example: "please take it to floor 21"
     * @param string|null $dropoff_contact_given_name example: "John"
     * @param string|null $dropoff_contact_family_name example: "Doe"
     * @param bool|null $dropoff_contact_send_notifications
     * @param int|null $order_value example: 5555
     * @param string|null $currency example: "USD"
     * @param CancellationReasonEnum|null $cancellation_reason
     * @param Carbon|null $updated_at example: "2022-02-01T23:18:22.791883Z"
     * @param Carbon|null $pickup_time_estimated example: "2022-02-01T23:18:22.791883Z"
     * @param Carbon|null $pickup_time_actual example: "2022-02-01T23:18:22.791883Z"
     * @param Carbon|null $dropoff_time_estimated example: "2022-02-01T23:18:22.791883Z"
     * @param Carbon|null $dropoff_time_actual example: "2022-02-01T23:18:22.791883Z"
     * @param int|null $fee example: 975
     * @param int|null $tip example: 230
     * @param string|null $support_reference example: "1343593362"
     * @param string|null $tracking_url example: "https://doordash.com/drive/portal/track/53904a0b-18cd-4308-b6dc-1d83932d7990"
     * @param bool|null $contactless
     */
    public function __construct(
        public readonly Carbon $created_at,
        public readonly string $event_name,
        public readonly string $external_delivery_id,
        public readonly ?int $dasher_id = null,
        public readonly ?string $dasher_name = null,
        public readonly ?string $dasher_dropoff_phone_number = null,
        public readonly ?string $dasher_pickup_phone_number = null,
        public readonly ?DasherLocation $dasher_location = null,
        public readonly ?string $dasher_vehicle_make = null,
        public readonly ?string $dasher_vehicle_model = null,
        public readonly ?string $dasher_vehicle_year = null,
        public readonly ?string $pickup_address = null,
        public readonly ?string $pickup_phone_number = null,
        public readonly ?string $pickup_instructions = null,
        public readonly ?string $pickupReferenceTag = null,
        public readonly ?string $pickup_external_business_id = null,
        public readonly ?string $pickup_external_store_id = null,
        public readonly ?string $dropoff_address = null,
        public readonly ?string $dropoff_phone_number = null,
        public readonly ?string $dropoff_instructions = null,
        public readonly ?string $dropoff_contact_given_name = null,
        public readonly ?string $dropoff_contact_family_name = null,
        public readonly ?bool $dropoff_contact_send_notifications = true,
        public readonly ?int $order_value = null,
        public readonly ?string $currency = null,
        public readonly ?CancellationReasonEnum $cancellation_reason = null,
        public readonly ?Carbon $updated_at = null,
        public readonly ?Carbon $pickup_time_estimated = null,
        public readonly ?Carbon $pickup_time_actual = null,
        public readonly ?Carbon $dropoff_time_estimated = null,
        public readonly ?Carbon $dropoff_time_actual = null,
        public readonly ?int $fee = null,
        public readonly ?int $tip = null,
        public readonly ?string $support_reference = null,
        public readonly ?string $tracking_url = null,
        public readonly ?bool $contactless = false
    ){}
}

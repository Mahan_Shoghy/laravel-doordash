<?php

namespace MahanShoghy\LaravelDoordash\App\Webhook\Model;

use Carbon\Carbon;
use MahanShoghy\LaravelDoordash\App\Drive\Enums\CancellationReasonEnum;
use MahanShoghy\LaravelDoordash\App\Drive\Parser\DasherLocationParser;

class PayloadParser
{
    private Payload $payload;

    public function __construct(array $data)
    {
        $dasher_location = (empty($data['dasher_location']))
            ? null
            : (new DasherLocationParser($data['dasher_location']))->get();

        $updated_at = (empty($data['updated_at']))
            ? null
            : Carbon::parse($data['updated_at']);

        $pickup_time_estimated = (empty($data['pickup_time_estimated']))
            ? null
            : Carbon::parse($data['pickup_time_estimated']);

        $pickup_time_actual = (empty($data['pickup_time_actual']))
            ? null
            : Carbon::parse($data['pickup_time_actual']);

        $dropoff_time_estimated = (empty($data['dropoff_time_estimated']))
            ? null
            : Carbon::parse($data['dropoff_time_estimated']);

        $dropoff_time_actual = (empty($data['dropoff_time_actual']))
            ? null
            : Carbon::parse($data['dropoff_time_actual']);

        $cancellation_reason = (empty($data['cancellation_reason']))
            ? null
            : CancellationReasonEnum::getFrom($data['cancellation_reason']);

        $this->payload = new Payload(
            Carbon::parse($data['created_at']),
            $data['event_name'],
            $data['external_delivery_id'],
            $data['dasher_id'] ?? null,
            $data['dasher_name'] ?? null,
            $data['dasher_dropoff_phone_number'] ?? null,
            $data['dasher_pickup_phone_number'] ?? null,
            $dasher_location,
            $data['dasher_vehicle_make'] ?? null,
            $data['dasher_vehicle_model'] ?? null,
            $data['dasher_vehicle_year'] ?? null,
            $data['pickup_address'] ?? null,
            $data['pickup_phone_number'] ?? null,
            $data['pickup_instructions'] ?? null,
            $data['pickupReferenceTag'] ?? null,
            $data['pickup_external_business_id'] ?? null,
            $data['pickup_external_store_id'] ?? null,
            $data['dropoff_address'] ?? null,
            $data['dropoff_phone_number'] ?? null,
            $data['dropoff_instructions'] ?? null,
            $data['dropoff_contact_given_name'] ?? null,
            $data['dropoff_contact_family_name'] ?? null,
            $data['dropoff_contact_send_notifications'] ?? null,
            $data['order_value'] ?? null,
            $data['currency'] ?? null,
            $cancellation_reason,
            $updated_at,
            $pickup_time_estimated,
            $pickup_time_actual,
            $dropoff_time_estimated,
            $dropoff_time_actual,
            $data['fee'] ?? null,
            $data['tip'] ?? null,
            $data['support_reference'] ?? null,
            $data['tracking_url'] ?? null,
            $data['contactless'] ?? null
        );
    }

    public function get(): Payload
    {
        return $this->payload;
    }
}

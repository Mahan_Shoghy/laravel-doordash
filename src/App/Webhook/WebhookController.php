<?php

namespace MahanShoghy\LaravelDoordash\App\Webhook;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use MahanShoghy\LaravelDoordash\App\Webhook\Jobs\ProcessDoordashWebhookJob;
use MahanShoghy\LaravelDoordash\App\Webhook\Model\PayloadParser;

class WebhookController
{
    public function __invoke(Request $request): JsonResponse
    {
        $payload = (new PayloadParser($request->all()))->get();

        dispatch(new ProcessDoordashWebhookJob($payload));

        return response()->json();
    }
}

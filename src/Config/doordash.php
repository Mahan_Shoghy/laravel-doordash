<?php


return [
    /**
     * These values are obtained from the Doordash Developer Portal.
     * Make sure to set these values in your environment file.
     */
    'developer_id' => env('DOORDASH_DEVELOPER_ID'), // Your Doordash Developer ID

    'key_id' => env('DOORDASH_KEY_ID'), // Your Doordash Key ID

    'signing_secret' => env('DOORDASH_SIGNING_SECRET'), // Your Doordash Signing Secret

    'webhook_auth_token' => env('DOORDASH_WEBHOOK_AUTH_TOKEN'), // Doordash authorization token

    /**
     * List of available jobs for webhook events.
     * You can find a list of available events in the Doordash documentation:
     * https://developer.doordash.com/en-US/docs/drive/reference/webhooks#events
     * Use UPPER_CASE naming for specified events, like the example below.
     * Uncomment and set the desired event handler class.
     */
    'jobs' => [
        // Example: 'DASHER_CONFIRMED' => DasherConfirmedJob::class,
    ]
];

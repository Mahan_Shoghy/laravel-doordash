<?php

namespace MahanShoghy\LaravelDoordash;

use Exception;
use Illuminate\Http\Client\Response;
use Throwable;

class DoordashException extends Exception
{
    private array $field_errors = [];
    private string $str_field_errors = '';
    private string $error_code;
    private string $error_message;

    public function __construct(Response $response, int $code = 500, ?Throwable $previous = null)
    {
        $this->setProperties($response);
        $message = "{$this->getErrorCode()} - {$this->getErrorMessage()}";

        $field_errors = $this->getStrFieldErrors();
        if ($field_errors){
            $message .= " - $field_errors";
        }

        parent::__construct($message, $code, $previous);
    }

    /**
     * Set error properties based on response
     *
     * @param Response $response
     * @return void
     */
    private function setProperties(Response $response): void
    {
        $response = $response->body();
        $response = json_decode($response, true);

        $this->error_code = $response['code'];
        $this->error_message = $response['message'];

        if (!empty($response['field_errors'])){
            foreach ($response['field_errors'] as $field){

                $this->str_field_errors .= "{{$field['field']}: {$field['error']}}";

                $this->field_errors[$field['field']][] = $field['error'];
            }
        }
    }

    /**
     * Get array of errors based on fields
     *
     * @return array
     */
    public function getFieldErrors(): array
    {
        return $this->field_errors;
    }


    /**
     * Get array of errors based on fields
     *
     * @return string
     */
    public function getStrFieldErrors(): string
    {
        return $this->str_field_errors;
    }

    /**
     * Get response error code
     *
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->error_code;
    }

    /**
     * Get response error message
     *
     * @return string
     */
    public function getErrorMessage(): string
    {
        return $this->error_message;
    }
}

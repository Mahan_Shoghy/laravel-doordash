<?php

namespace MahanShoghy\LaravelDoordash;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use MahanShoghy\LaravelDoordash\App\Webhook\Commands\GenerateAuthTokenCommand;
use MahanShoghy\LaravelDoordash\App\Webhook\Middlewares\WebhookAuthMiddleware;
use MahanShoghy\LaravelDoordash\App\Webhook\WebhookController;

class DoordashServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind('Delivery', \MahanShoghy\LaravelDoordash\App\Drive\Delivery::class);

        $loader = AliasLoader::getInstance();
        $loader->alias('Delivery', \MahanShoghy\LaravelDoordash\App\Drive\Facades\Delivery::class);

        $this->mergeConfigFrom(__DIR__.'/Config/doordash.php', 'doordash');

        $this->registerWebhook();
    }

    public function boot(): void
    {
        if ($this->app->runningInConsole()) {

            $this->publishes([
                __DIR__.'/Config/doordash.php' => config_path('doordash.php')
            ], 'config');
        }
    }

    private function registerWebhook(): void
    {
        Route::macro('doordashWebhooks', function (string $url, string $method = 'post') {
            return Route::$method($url, WebhookController::class)->middleware(WebhookAuthMiddleware::class);
        });

        $this->commands([GenerateAuthTokenCommand::class]);
    }
}

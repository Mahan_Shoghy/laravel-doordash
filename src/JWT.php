<?php

namespace MahanShoghy\LaravelDoordash;

use Exception;

class JWT
{
    private static ?JWT $instance = null;

    private string $token;

    protected function __construct()
    {
        $this->generateToken();
    }

    protected function __clone() { }

    /**
     * @throws Exception
     */
    public function __wakeup()
    {
        throw new Exception("Cannot unserialize a singleton.");
    }

    public static function getInstance(): JWT
    {
        if (!self::$instance){
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function generateToken(): void
    {
        $header = json_encode([
            'alg' => 'HS256',
            'typ' => 'JWT',
            'dd-ver' => 'DD-JWT-V1'
        ]);

        $payload = json_encode([
            'aud' => 'doordash',
            'iss' => config('doordash.developer_id'),
            'kid' => config('doordash.key_id'),
            'exp' => time() + 300,
            'iat' => time()
        ]);

        $base64UrlHeader = base64UrlEncode($header);
        $base64UrlPayload = base64UrlEncode($payload);

        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, base64UrlDecode(config('doordash.signing_secret')), true);
        $base64UrlSignature = base64UrlEncode($signature);

        $this->token = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;
    }

    public function getToken(): string
    {
        return $this->token;
    }
}
